package com.jeux_dames.App;

import com.jeux_dames.Classes.ClientPartie;
import com.jeux_dames.Classes.InterfaceJeuDeDame;
import com.jeux_dames.Controllers.MenuController;
import com.sun.deploy.util.SessionState;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Timer;
import java.util.TimerTask;

public class JeuDeDamesApp extends Application {

    private static final Logger log = LoggerFactory.getLogger(JeuDeDamesApp.class);
    private ClientPartie c;

    public static void main(String[] args) throws Exception {
        launch(args);
    }

    public void start(Stage stage) throws Exception {
        this.c = new ClientPartie();
        c.controllerJeuDame = new MenuController(c);

        Scene scene = new Scene(c.controllerJeuDame);
        scene.getStylesheets().add("/styles/styles.css");

        stage.setTitle("Jeu de dames");
        stage.setScene(scene);
        stage.setWidth(820);
        stage.setHeight(800);
        stage.setResizable(true);
        stage.show();

        Timer action = new java.util.Timer();
        action = new java.util.Timer();
        action.schedule(new TimerTask() {
            //toutes les 100 milisecondes il vérifie l'état de l'arbre
            public void run() {
                try {

                } catch (NullPointerException e) {
                    System.out.println("La liste de client est vide");
                }
            }
        }, 0, 5000);

    }


}
