package com.jeux_dames.Classes;

import javafx.scene.shape.Rectangle;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Gatien on 17/03/2017.
 * wmic process where "name like '%java%'" delete
 */
public class Partie extends UnicastRemoteObject implements InterfaceJeuDeDame {
    //Joueur de la partie (seulement deux joueur aux dames pour une partie)
    private Joueur joueur_1;
    private Joueur joueur_2;
    private ArrayList<ClientDistant> clients;
    private int temps_partie; // temps de la partie en secondes
    private Joueur winner;
    private ArrayList<Case> cases; //Damier

    public Partie() throws RemoteException {

        clients = new ArrayList<ClientDistant>();
        cases = new ArrayList<Case>();
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                cases.add(new Case(i, j, true));
            }
        }
    }


    public boolean rejoindrePartie(String pseudo, ClientDistant clientDistant) throws RemoteException {
        if (this.joueur_1 == null) {
            this.joueur_1 = new Joueur(pseudo, 1, clientDistant);
            this.joueur_1.setClient(clientDistant);
            registerClient(clientDistant);
            this.joueur_1.getPions_joueur().addAll(creerPion(1));

            return true;
        } else if (this.joueur_2 == null) {
            this.joueur_2 = new Joueur(pseudo, 2, clientDistant);
            this.joueur_2.setClient(clientDistant);
            registerClient(clientDistant);

            this.joueur_2.getPions_joueur().addAll(creerPion(2));
            this.joueur_1.debutTour();
            return true;
        } else {
            return false;
        }
    }

    public boolean continuerPartie(String pseudo, ClientDistant clientDistant) throws RemoteException {
        if (this.joueur_1 != null && joueur_2 != null) {
            //Le mieux aurait été de comarere avec quelque chose de stable mais bon ...
            if (this.joueur_1.getNom_joueur().compareTo(pseudo) == 0) {
                this.joueur_1.setClient(clientDistant);
                System.out.println("OK J1");
                registerClient(clientDistant);
                mettreAjourDamierClient();
                return true;
            }

            if (this.joueur_2.getNom_joueur().compareTo(pseudo) == 0) {
                this.joueur_2.setClient(clientDistant);
                System.out.println("OK J2");
                registerClient(clientDistant);
                mettreAjourDamierClient();
                return true;
            }
        }
        return false;
    }

    /**
     * Cette fonction vérifie si un pion est présent sur une case et le sélectionne si possible
     *
     * @param x coord x de la case
     * @param y coord y de la case
     * @param c Client qui fait la demande
     * @return
     * @throws RemoteException
     */
    //Cette fonction retourne le droit ou non de sélectionner un pion
    public boolean selectionnerPion(int x, int y, ClientDistant c) throws RemoteException {
        if (this.getTourJoueur().getClient() != null) {
            //On récupère le joueur qui a le pion
            Case caseRecherchee = this.getCase_x_y(x, y);
            //Joueur sur la case
            Joueur j = getJoueurCase(caseRecherchee);
            //Joueur qui à la main sur le damier
            Joueur tour = getTourJoueur();

            //On cherche si le pion selectionné appartien bien au joueur du tour actuel(getJoueurCase == getTourJoueur)
            //On regarde également si le demandeur distant correspond bien au joueur du tour actuel
            if (j.getNumero_joueur() == tour.getNumero_joueur() && tour.getClient().equals(c) && tour.getNbPionPris() <= 0) {
                tour.setPion_select(tour.getPion(caseRecherchee));
                return true;
            } else {
                return false;
            }
        }

        return false;
    }

    /**
     * @param c      Case que l'on souhaite sélectionner pouir une action
     * @param client Client qui demande l'action
     * @return retourne vrai ou faux (vrai, l'action a eu lieue, faux elle n'a pas eu lieu)
     * @throws RemoteException
     */
    //Cette fonction permet de sélectionner une case en fonction des possibilités
    public boolean selectionnerCase(Case c, ClientDistant client) throws RemoteException {
        if (this.getTourJoueur().getClient() != null) {
            if (this.getTourJoueur().getClient().equals(client) && getTourJoueur().getPion_select() != null) {
                switch (getActionPossible(c)) {
                    case 1:
                        deplacerPion(c);
                        return true;
                    case 2:
                        //on mange le pion
                        mangePion(c);
                        //puis on le déplace
                        deplacerPion(c);
                        return true;

                }
            }
        }
        return false;
    }

    /**
     * @param c Case sur laquelle on veut tester l'action possible (par rapport au pion selectionné)
     * @return
     */
    //Fonction qui gère les actions des pions
    public int getActionPossible(Case c) {
        if (!getTourJoueur().getPion_select().isIs_dame()) {
            return getActionPossiblePion(c);
        } else {
            //on retourne le mouvement possible pour une dame
            //// TODO: 27/04/2017 Voir pour faire une fonction qui regarde si la dame peut prendre un pion ou avancer ou rien
            //--> changer getActionPion
            //return getActionPossiblePion(c);
            return getActionPossibleDame(c);
        }
    }

    /**
     * @param c case sur laquelle on veut tester l'action possible par rapport au pion selectionné
     * @return 1 si déplacement 2 si prise -1 si rien du tout
     */
    //Fonction qui retourne l'action possible sur une case
    public int getActionPossiblePion(Case c) {
        int action = -1;
        int sousX = (c.getPositionI() - getTourJoueur().getPion_select().getCurrent_case().getPositionI());
        int sousY = (c.getPositionJ() - getTourJoueur().getPion_select().getCurrent_case().getPositionJ());

        //On vérifie qu'on est bien dans un déplacement diagonal et dans le sens du joueur (on ne recule pas)
        //RAPPEL Joueur 2 (blanc) descend et joueur 1 (noir) Monte
        //Condition sur le sens du déplacement
        //((getTourJoueur().getNumero_joueur() == 1 && sousY>0) || (getTourJoueur().getNumero_joueur() == 2 && sousY<0))
        if (
            //Condition de diagonalité du déplacement
                Math.abs(sousX) - Math.abs(sousY) == 0 &&
                        //L'action doit se baser sur un pion
                        getTourJoueur().getPion_select() != null
                        //La case doit-être libre
                        && getJoueurCase(c).getNumero_joueur() == -1
                ) {

            //On regarde ensuite si le déplacement est de 1 ou de 2
            switch (c.getPositionJ() - getTourJoueur().getPion_select().getCurrent_case().getPositionJ()) {
                //Si c'est 1 le joueur 1 peut se déplacer
                case 1:
                    //On peut se déplacer
                    if (getTourJoueur().getNumero_joueur() == 1 && getTourJoueur().getNbPionPris() <= 0) {
                        action = 1;
                    }
                    break;
                //Le joueur 2 se déplace
                case -1:
                    //On peut se déplacer
                    if (getTourJoueur().getNumero_joueur() == 2 && getTourJoueur().getNbPionPris() <= 0) {
                        action = 1;
                    }
                    break;
                //On prend un pion
                case 2:
                    //On peut prendre un pion seulement s'il y en a un ...
                    action = 3 - countNbPionDiag(c);
                    break;
                case -2:
                    //On peut prendre un pion seulement s'il y en a un ...
                    action = 3 - countNbPionDiag(c);
                    break;
                default:
                    //Le pion ne peut rien faire
                    action = -1;
                    break;
            }
        }


        return action;
    }

    /**
     * Cette fonction indique si une prise de pion est possible à partir du pion actuel(sélectionné)
     * Si la fonction renvoie true, le joueur peut continuer à prendre des pions
     */
    public boolean hasPionPrise(Case case_pion_actuel) {
        boolean hasPrise = true;

        //Il y a 4 diagonales haut gauche, haut droite, bas gauche, bas droite
        int diag = 1;

        while (diag <= 4) {


            int x = case_pion_actuel.getPositionI();
            int y = case_pion_actuel.getPositionJ();


            //Parcours de la diagonale gauche haut
            //Tant qu'on atteint pas les bord du plateau

            while (x != -1 && y != -1 && x!=10 && y!=10) {
                switch(diag){

                    //haut gauche
                    case 1:
                        x--;
                        y++;
                        break;
                    //haut droite
                    case 2:
                        x++;
                        y++;
                        break;

                    //bas gauche
                    case 3:
                        x--;
                        y--;
                        break;
                    //bas droite
                    case 4:
                        x++;
                        y--;
                        break;
                }

                if (Math.abs(getActionPossible(new Case(x, y, true))) == 2 && x != -1 && y != -1 && x!=10 && y!=10) {
                    hasPrise = false;
                }
            }

            diag++;
        }

        return hasPrise;

    }

    public int countNbPionDiag(Case c) {
        //Nombre de pions adverses trouvé sur la diagonale
        int nbPionAdv = 0;

        //Nombre de pion allié trouvés sur la diagonale
        int nbPionAl = 0;


        //On récupère le pion sélectionné
        Case selectPion = getTourJoueur().getPion_select().getCurrent_case();

        //On calcul les directions
        int sousX = (c.getPositionI() - getTourJoueur().getPion_select().getCurrent_case().getPositionI());
        int sousY = (c.getPositionJ() - getTourJoueur().getPion_select().getCurrent_case().getPositionJ());


        //Case utilisé pour la comparaison
        Case caseTemp = new Case(-10, -10, true);

        for (int i = 1; i <= Math.abs(sousY); i++) {

            caseTemp = getCaseDirection(sousX, sousY, i);

            if (getJoueurCase(caseTemp).getNumero_joueur() == getNonTourJoueur().getNumero_joueur()) {
                nbPionAdv++;
            } else if (getJoueurCase(caseTemp).getNumero_joueur() == getTourJoueur().getNumero_joueur()) {
                nbPionAl++;
            }
        }

        //Si un pion allier est dans la diagonale on ne peut pas avancer
        if (nbPionAl > 0 || nbPionAdv > 1) {
            return 99;
        }
        //Une dame ne peut prendre qu'un seul pion
        else if (nbPionAdv == 1) {
            return 1;
        }
        //Si y a pas de pion, on peut effectuer un déplacement
        else {
            if(getTourJoueur().getNbPionPris() <= 0) {
                return 0;
            }
            else{
                return 99;
            }
        }
    }

    //Fonction qui retourne l'action possible sur une case
    public int getActionPossibleDame(Case c) {
        int action = -1;
        int sousX = (c.getPositionI() - getTourJoueur().getPion_select().getCurrent_case().getPositionI());
        int sousY = (c.getPositionJ() - getTourJoueur().getPion_select().getCurrent_case().getPositionJ());

        //On vérifie qu'on est bien dans un déplacement diagonal et que la case d'arrivée n'est pas prise
        if (Math.abs(sousX) - Math.abs(sousY) == 0 && getTourJoueur().getPion_select() != null && getJoueurCase(c).getNumero_joueur() == -1) {

            switch (countNbPionDiag(c)) {
                case 99:
                    return 99;
                case 1:
                    return 2; //code 1, on peut prendre un pion
                case 0:
                    return 1; //Code 2, on peut faire un déplacement
                default:
                    return -1;//Code rien du tout on ne peut rien faire
            }
        }


        return action;
    }

    //Cette méthode retourne le joueur d'un pion
    public Joueur getJoueurCase(Case c) {

        if (joueur_1 != null) {
            if (joueur_1.hasPion(c)) {
                return joueur_1;
            }
        }

        if (joueur_2 != null) {
            if (joueur_2.hasPion(c)) {
                return joueur_2;
            }
        }

        //Si aucun joueur ne possède le pion, on retourne un joueur null
        return new Joueur("null", -1, null);
    }

    public void checkIfDame(Pion selectionné, Case destination) throws RemoteException {
        if (!selectionné.isIs_dame()) {
            if (getTourJoueur().getNumero_joueur() == 1) { //Joueur 1
                if (destination.getPositionJ() == 9) {
                    selectionné.setIs_dame(true);
                }
            } else if (getTourJoueur().getNumero_joueur() == 2) { //Joueur2
                if (destination.getPositionJ() == 0) {
                    selectionné.setIs_dame(true);
                }
            }
        }
    }

    // Fonction de déplacement d'un pion. Param : case de destination cliquée par le joueur. Met à jour les damiers des deux clients à la fin du traitement.
    public void deplacerPion(Case destination) throws RemoteException {
        //Le pion prend la nouvelle case
        Pion selectionné = getTourJoueur().getPion_select();

        selectionné.setCurrent_case(destination);
        //// TODO: 20/04/2017 faire la fonction qui regarde si le pion devient une dame
        checkIfDame(selectionné, destination);

        Case case_pion_actuel = getTourJoueur().getPion_select().getCurrent_case();

        if (hasPionPrise(case_pion_actuel) || (!hasPionPrise(case_pion_actuel) && getTourJoueur().getNbPionPris() <= 0)) {
            //C'est une action donc on change de tour, le tour est terminé
            changerTour();
            //On supprime le pion de la selection
            getTourJoueur().deselectPion();
        }

        //On met à jour les damier des client
        mettreAjourDamierClient();
    }

    public void changerTour() { // fonction qui passe le tour au joueur adversaire
        if (this.joueur_1.isSon_tour()) {
            System.out.println("Tour du Joueur 2 (les noirs)");
            joueur_1.setSon_tour(false);
            joueur_2.setSon_tour(true);
            joueur_1.resetPionPris();
            joueur_2.resetPionPris();
        } else {
            System.out.println("Tour du Joueur 1 (les blancs)");
            joueur_1.setSon_tour(true);
            joueur_2.setSon_tour(false);
            joueur_2.resetPionPris();
            joueur_1.resetPionPris();
        }
    }

    public ArrayList<Pion> creerPion(int joueur) throws RemoteException {
        ArrayList<Pion> liste_pions = new ArrayList<Pion>();
        int startPositionJ;
        int endPositionJ;
        if (joueur == 1) {
            startPositionJ = 0;
            endPositionJ = 3;
        } else {
            startPositionJ = 6;
            endPositionJ = 9;
        }
        for (int posJ = startPositionJ; posJ <= endPositionJ; posJ++) {
            for (int posI = 0; posI < 10; posI++) {
                if (posI % 2 == posJ % 2) {
                    Pion pion = new Pion(getCase_x_y(posI, posJ));
                    liste_pions.add(pion);
                }
            }
        }
        return liste_pions;
    }

    // Enregistrement de la partie courante au format json
    public void savePartie() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        List<Joueur> joueurs = new ArrayList<Joueur>();
        joueurs.add(this.joueur_1);
        joueurs.add(this.joueur_2);
        mapper.writeValue(new File("save.json"), joueurs);
    }

    // Chargement d'une partie sauvegardée
    public void loadPartie() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        List<Joueur> joueurs = mapper.readValue(new File("save.json"), new TypeReference<List<Joueur>>() {
        });
        if (joueurs.get(0).getNumero_joueur() == 1) {
            this.joueur_1 = joueurs.get(0);
            this.joueur_2 = joueurs.get(1);
        } else {
            this.joueur_1 = joueurs.get(1);
            this.joueur_2 = joueurs.get(0);
        }
    }

    public int getNbClient() {
        return this.clients.size();
    }

    public ArrayList<Case> getDamier() {
        return this.cases;
    }

    public Joueur getJoueur_1() {
        return joueur_1;
    }


    public Joueur getJoueur_2() {
        return joueur_2;
    }


    public int getTemps_partie() {
        return temps_partie;
    }

    public void setTemps_partie(int temps_partie) {
        this.temps_partie = temps_partie;
    }


    public ArrayList<ClientDistant> getClients() {
        return clients;
    }


    public ArrayList<Case> getCases() {
        return cases;
    }

    public void avancerPion() {

    }

    public Joueur getTourJoueur() {
        if (this.joueur_1.isSon_tour()) {
            return joueur_1;
        } else {
            return joueur_2;
        }
    }

    public void actualiserAffichageClient() throws RemoteException {
        this.mettreAjourClient();
    }

    public void registerClient(ClientDistant client) throws RemoteException {
        this.clients.add(client);
        System.out.println("Nouveau client");
    }

    public void mettreAjourClient() throws RemoteException {
        for (ClientDistant c : this.clients) {
            c.update();
        }
    }

    public void mettreAjourDamierClient() throws RemoteException {
        for (ClientDistant c : this.clients) {
            c.updateDamier();
        }

    }

    public Case getCase_x_y(int x, int y) { // Utile uniquement lors de la création du damier
        for (Case c : cases) {
            if (c.getPositionI() == x && c.getPositionJ() == y) {
                return c;
            }
        }
        return null;
    }

    public Case getCase_rectangle(Rectangle rectangle) { // Permet de trouver une Case en fonction d'un Rectangle.
        for (Case c : cases) {
            if (c.getRectangleFX() == rectangle) {
                return c;
            }
        }
        return null;
    }

    //Fonction qui supprime tous les pions entre entre un point A et un point B
    //La fonction s'appui sur le joueur en cours et sur le pion selectionné
    public void mangePion(Case c) {
        //On calcul les directions
        int sousX = (c.getPositionI() - getTourJoueur().getPion_select().getCurrent_case().getPositionI());
        int sousY = (c.getPositionJ() - getTourJoueur().getPion_select().getCurrent_case().getPositionJ());

        //Indice du pion devant Ítre supprimé
        int indPionSup = -99;


        for (int i = 1; i <= Math.abs(sousY); i++) {


            indPionSup = getNonTourJoueur().getIndPion(getCaseDirection(sousX, sousY, i));

            if (indPionSup > -1) {
                getNonTourJoueur().supprimerPion(indPionSup);

                //On incrémente le nombre de pions pris
                this.getTourJoueur().addPionPris();
            }

        }


    }


    //Retourne la case correspondant à une direction donnée
    public Case getCaseDirection(int sousX, int sousY, int i) {
        Case caseTemp = new Case(-10, -10, true);
        //On récupère le pion sélectionné
        Case selectPion = getTourJoueur().getPion_select().getCurrent_case();


        if (sousY < 0 && sousX < 0) {
            caseTemp = new Case(selectPion.getPositionI() - i, selectPion.getPositionJ() - i, true);
        }

        if (sousY < 0 && sousX > 0) {
            caseTemp = new Case(selectPion.getPositionI() + i, selectPion.getPositionJ() - i, true);
        }

        if (sousY > 0 && sousX > 0) {
            caseTemp = new Case(selectPion.getPositionI() + i, selectPion.getPositionJ() + i, true);
        }

        if (sousY > 0 && sousX < 0) {
            caseTemp = new Case(selectPion.getPositionI() - i, selectPion.getPositionJ() + i, true);
        }

        return caseTemp;
    }

    //Retourne le joueur qui n'a pas la main sur le jeu
    public Joueur getNonTourJoueur() {
        if (this.joueur_1.isSon_tour()) {
            return joueur_2;
        } else {
            return joueur_1;
        }
    }

    public Joueur getJoueur(ClientDistant clientDistant) {
        if (this.joueur_1.getClient().equals(clientDistant)) {
            return joueur_1;
        } else if (this.joueur_2.getClient().equals(clientDistant)) {
            return joueur_2;
        } else return null;
    }
}

