package com.jeux_dames.Classes;

import com.jeux_dames.Controllers.MenuController;
import com.sun.deploy.util.SessionState;
import javafx.application.Platform;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.server.RemoteRef;
import java.rmi.server.UnicastRemoteObject;
import java.io.Serializable;

/**
 * Created by Anthony on 19/03/2017.
 */
public class ClientPartie extends UnicastRemoteObject implements ClientDistant {

    //public static String port[];

    public static int clients = 1;
    public MenuController controllerJeuDame;
    public Remote partieDame;

    public void update() throws RemoteException {
        Platform.runLater(new Runnable() {
            public void run() {
                try {
                    controllerJeuDame.refreshJoueur();
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    public void updateDamier() throws RemoteException {
        Platform.runLater(new Runnable() {
            public void run() {
                try {
                    controllerJeuDame.refreshDamier();
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public ClientPartie() throws RemoteException {
    }

    public void connexionServeur(String adress, String port) {
        ClientPartie.clients++;

        System.out.println("Lancement du client");
        if (System.getSecurityManager() == null) {
            System.setProperty("java.security.policy", "file:./src/main/resources/files/securite.policy");
            System.setProperty("java.rmi.server.hostname", adress);
            System.setSecurityManager(new SecurityManager());
        }
        try {

            System.out.println("Le client rejoint : rmi://"+adress+":"+port+"/PartieDame" );
            this.partieDame = Naming.lookup("rmi://"+adress+":"+port+"/PartieDame");

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (NotBoundException e) {
            e.printStackTrace();
        }
        System.out.println("Fin du client");
    }


}
