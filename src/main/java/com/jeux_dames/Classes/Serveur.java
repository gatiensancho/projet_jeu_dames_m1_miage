package com.jeux_dames.Classes;

import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.util.Scanner;

import static java.lang.Integer.parseInt;

/**
 * Created by Anthony on 21/03/2017.
 */
public class Serveur {

    public static int partie =0;

    public static Partie jeu;

    public static boolean lancerServeur(int port, String adresse, String objetName) {
        try {

            String url = "rmi://" + InetAddress.getLocalHost().getHostAddress()+":"+port + "/" + objetName;

            //On kill le serveur si une partie est en cours
            if(partie > 0) {
                System.out.println("Suppression de l'ancienne partie");
                Naming.unbind(url);

            }

            if(partie == 0) {

                System.out.println("Créationde la nouvelle partie sur le port "+port);
                LocateRegistry.createRegistry(port);
            }


            System.out.println("Mise en place du Security Manager ...");

            if (System.getSecurityManager() == null) {
                System.setProperty("java.security.policy", "file:./src/main/resources/files/securite.policy");
                System.setProperty("java.rmi.server.hostname", InetAddress.getLocalHost().getHostAddress());
                System.setSecurityManager(new SecurityManager());
            }

            jeu = new Partie();


            System.out.println("Lancement du serveur sur l'url : " + url);

            Naming.rebind(url, jeu);


            System.out.println("Serveur lancé");

            partie ++;
            return true;
            } catch (RemoteException e) {
                e.printStackTrace();
                return false;
            } catch (MalformedURLException e) {
                e.printStackTrace();
                return false;
            } catch (UnknownHostException e) {
                e.printStackTrace();
                return false;

            } catch (NotBoundException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static void main(String[] args) throws Exception {
        lancerServeur(parseInt(args[0]), args[1], args[2]);


        new Thread() {
            public void run() {
                //stock le choix de stockage des données de l'utilisateur
                String reponse = "";
                String commande = "";
                String params = "";
                Scanner sc = new Scanner(System.in);

                System.out.println("Liste des commandes :");
                System.out.println("--Afficher le nombre de client connectés : 1 ");
                //tant que l'utilisateur fait le mauvais choix on l'embète
                while (reponse.compareTo("fin") != 0) {

                    reponse = sc.nextLine();

                    switch (parseInt(reponse)) {
                        case 1:
                            System.out.println("Nombre de clients " + jeu.getClients().size());
                            break;

                    }
                }
            }

        }.start(); //initialisation d'un scanner pour récupèrer le choix de l'utilisateur
    }


}
