package com.jeux_dames.Classes;

import java.io.Serializable;

/**
 * Created by Gatien on 17/03/2017.
 */
public class Pion implements Serializable {
    private Case current_case; // case courante
    private boolean is_available=true; // pion déjà mangé ou non. True par défaut.
    private boolean is_dame = false; // pion transformé en dame ou non. False par défaut
    private boolean is_selected = false; // pion selectionné avant déplacement. False par défaut.

    // La couleur du pion (rouge ou noir) est calculée suivant le joueur [J1=noir|J2=rouge]

    public Pion(Case current_case) {
        this.current_case = current_case;
    }

    // Constructeur par défaut pour le chargement des parties
    public Pion() {
    }

    public Case getCurrent_case() {
        return current_case;
    }

    public void setCurrent_case(Case current_case) {
        this.current_case = current_case;
    }

    public boolean isIs_available() {
        return is_available;
    }

    public void setIs_available(boolean is_available) {
        this.is_available = is_available;
    }

    public boolean isIs_dame() {
        return is_dame;
    }

    public void setIs_dame(boolean is_dame) {
        this.is_dame = is_dame;
    }

    public boolean isIs_selected() {
        return is_selected;
    }

    public void setIs_selected(boolean is_selected) {
        this.is_selected = is_selected;
    }
}
