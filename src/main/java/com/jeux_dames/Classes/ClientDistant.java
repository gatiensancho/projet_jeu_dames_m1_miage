package com.jeux_dames.Classes;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.server.RemoteRef;

/**
 * Created by Anthony on 20/03/2017.
 */
public interface ClientDistant extends Remote {

     void update() throws RemoteException;

     void updateDamier() throws RemoteException;


}
