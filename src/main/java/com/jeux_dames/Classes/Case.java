package com.jeux_dames.Classes;

import javafx.scene.shape.Rectangle;
import org.codehaus.jackson.annotate.JsonIgnore;

import java.io.Serializable;

/**
 * Created by Gatien on 17/03/2017.
 */
public class Case implements Serializable {
    private int positionI;
    private int positionJ;
    private boolean is_available; // jeu de dames : une case sur deux est jouable

    @JsonIgnore
    private Rectangle rectangleFX; // objet rectangle JavaFX

    // Constructeur par défaut pour le chargement des parties
    public Case() {
    }

    public Case(int positionI, int positionJ, boolean is_available) {
        this.positionI = positionI;
        this.positionJ = positionJ;
        this.is_available = is_available;
    }

    public boolean equals(Case c){
        if(c.getPositionI() == this.getPositionI() && this.getPositionJ() == c.positionJ){
            return true;
        }
        else{
            return false;
        }
    }

    public int getPositionI() {
        return positionI;
    }

    public int getPositionJ() {
        return positionJ;
    }

    public boolean isIs_available() {
        return is_available;
    }

    public void setIs_available(boolean is_available) {
        this.is_available = is_available;
    }


    public Rectangle getRectangleFX() {
        return rectangleFX;
    }

    public void setRectangleFX(Rectangle rectangleFX) {
        this.rectangleFX = rectangleFX;
    }

    public String toString(){
        String chaine = this.getPositionI()+";"+this.getPositionJ();
        return chaine;
    }

}
