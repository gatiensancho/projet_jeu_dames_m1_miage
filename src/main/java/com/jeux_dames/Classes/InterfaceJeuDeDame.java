package com.jeux_dames.Classes;

import javafx.scene.shape.Rectangle;

import java.io.IOException;
import java.lang.reflect.Array;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

/**
 * Created by Anthony on 18/03/2017.
 */
public interface InterfaceJeuDeDame extends Remote {
    void avancerPion() throws RemoteException;

    boolean rejoindrePartie(String pseudo, ClientDistant c) throws RemoteException;

    boolean continuerPartie(String pseudo, ClientDistant c) throws RemoteException;


    void actualiserAffichageClient() throws RemoteException;

    Joueur getJoueur_1() throws RemoteException;

    Joueur getJoueur_2() throws RemoteException;

    void registerClient(ClientDistant client) throws RemoteException;

    boolean selectionnerPion(int x, int y, ClientDistant c) throws RemoteException;

    ArrayList<Case> getDamier() throws RemoteException;

    Case getCase_x_y(int x, int y) throws RemoteException;

    boolean selectionnerCase(Case c, ClientDistant client) throws RemoteException;

    Case getCase_rectangle(Rectangle r) throws RemoteException;

    // Enregistrement de la partie courante
    void savePartie() throws IOException;

    // Load partie sauvegardée
    void loadPartie() throws IOException;

    Joueur getJoueur(ClientDistant clientDistant) throws RemoteException;
    //public int getNbClient();
}
