package com.jeux_dames.Classes;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Gatien on 17/03/2017.
 */
public class Joueur implements Serializable {
    @JsonProperty("pseudo")
    private String nom_joueur;

    @JsonProperty("numero")
    private int numero_joueur;

    @JsonIgnore
    private ClientDistant client;

    @JsonProperty("tour")
    private boolean son_tour = false; // tour du joueur actuellement ? False à la création du joueur

    @JsonProperty("pions")
    private ArrayList<Pion> pions_joueur;

    @JsonProperty("pion")
    private Pion pion_select; //sauvegarde le pion que le joueur à séléctionné durant un tour

    @JsonProperty("pion_pris")
    private int nbPionPris; // enregistre le nombre de pion pris dans le tour


    public void addPionPris(){
        this.nbPionPris ++;
    }

    public void resetPionPris(){
        this.nbPionPris=0;
    }

    public int getNbPionPris(){
        return this.nbPionPris;
    }

    // Constructeur par défaut pour le chargement des parties
    public Joueur() {
    }

    public Joueur(String nom_joueur, int numero, ClientDistant c) {
        this.pions_joueur = new ArrayList<Pion>();
        this.client=c;
        this.nom_joueur = nom_joueur;
        this.numero_joueur=numero;
    }
    //Cette fonction retourne vrai si le joueur à le pion
    public boolean hasPion(Case c){
        for (Pion p : this.pions_joueur){
            if(p.getCurrent_case().equals(c)){
                return true;
            }
        }
        return false;
    }

    //Cette fonction retourne un pion en fonction d'une case passée en paramètre
    public Pion getPion(Case c){
        for (Pion p : this.pions_joueur){
            if(p.getCurrent_case().equals(c)){
                return p;
            }
        }
        return null;
    }

    public void deselectPion() {
        this.pion_select = null;
    }

    public void setPion_select(Pion pion_select) {
        this.pion_select = pion_select;
    }

    public Pion getPion_select() {
        return pion_select;
    }

    public ClientDistant getClient() {
        return client;
    }

    public String getNom_joueur() {
        return nom_joueur;
    }

    public void setClient(ClientDistant c) {
        this.client = c;
    }

    public void debutTour(){
        this.son_tour=true;
    }

    public int getNumero_joueur(){
        return this.numero_joueur;
    }
    public void finTour(){
        this.son_tour=false;
    }
    public void setNom_joueur(String nom_joueur) {
        this.nom_joueur = nom_joueur;
    }

    public boolean isSon_tour() {
        return son_tour;
    }

    public void setSon_tour(boolean son_tour) {
        this.son_tour = son_tour;
    }

    public ArrayList<Pion> getPions_joueur() {
        return pions_joueur;
    }

    public void setPions_joueur(ArrayList<Pion> pions_joueur) {
        this.pions_joueur = pions_joueur;
    }

    public String toString(){
        String chaine = "Je suis le joueur "+ this.numero_joueur;
        return chaine;
    }

    public void afficherPions(){
        System.out.println(this.toString()+" mes pions :");
        for (Pion p: this.pions_joueur) {
            System.out.println("Pion sur la case "+ p.getCurrent_case().toString());
        }
    }

    //Cette fonction retourne l'indice d'un pion dans la liste
    //Retourne -1 si le pion n'est pas dans la liste
    public int getIndPion(Case c){
        int indice =0;

        for (Pion p : this.pions_joueur){
            if(p.getCurrent_case().equals(c)){
                return indice;
            }
            indice++;
        }
        return -1;
    }


    //Fonction qui supprime un pion de la partie
    public void supprimerPion(int indice){
        this.pions_joueur.remove(indice);
    }


}
