package com.jeux_dames.Controllers;

import com.jeux_dames.Classes.*;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.ImagePattern;
import javafx.scene.control.Alert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

import java.io.IOException;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.util.ArrayList;
import java.util.HashMap;

public class MenuController extends GridPane {
    private static final Logger log = LoggerFactory.getLogger(MenuController.class);
    private ClientPartie c;


    @FXML
    private TextField firstNameField;
    @FXML
    private TextField lastNameField;
    @FXML
    private Label messageLabel;
    @FXML
    private Label j1;
    @FXML
    private Label j2;
    @FXML
    private TextField pseudo;
    @FXML
    private TextField ipServeur;
    @FXML
    private TextField portServeur;
    @FXML
    private Group damier;

    private final ArrayList<Rectangle> array_rectangle = new ArrayList<Rectangle>();
    private final HashMap<String, Rectangle> map_rectangle = new HashMap<String, Rectangle>();
    private final int mesure_case = 40; //taille des cases

    public MenuController(ClientPartie c) {
        this.c = c;

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/views/Menu.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
        this.portServeur.setText("1099");
    }

    public void refreshJoueur() throws RemoteException {
        try {
            this.j1.setText(((InterfaceJeuDeDame) c.partieDame).getJoueur_1().getNom_joueur());
            this.j2.setText(((InterfaceJeuDeDame) c.partieDame).getJoueur_2().getNom_joueur());
        } catch (Exception e) {
            System.out.println("Aucun joueur disponible");
        }

    }

    public void refreshDamier() throws RemoteException {
        damier.getChildren().clear();
        int x = 0;
        int y = 9;
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                final Case case_courante = ((InterfaceJeuDeDame) c.partieDame).getCase_x_y(x, y);
                final Rectangle r = new Rectangle(mesure_case, mesure_case);
                r.setX(mesure_case * j);
                r.setY(mesure_case + (mesure_case * i));

                if ((j % 2 == 0 && i % 2 == 0) || (j % 2 != 0 && i % 2 != 0)) {
                    r.setFill(Color.web("#bc9359"));
                } else {
                    r.setFill(Color.web("#886c3b"));
                }
                damier.getChildren().add(r);

                genererEvent(case_courante, r);

                // On ajoute le Rectangle à l'arraylist
                map_rectangle.put(Integer.toString(x) + "-" + Integer.toString(y), r);
                array_rectangle.add(r);

                if (x != 9) {
                    x++;
                } else {
                    x = 0;
                    y--;
                }

            }
        }


        final Joueur j1 = ((InterfaceJeuDeDame) c.partieDame).getJoueur_1();
        final Joueur j2 = ((InterfaceJeuDeDame) c.partieDame).getJoueur_2();
        Image img;
        for (Pion p : j2.getPions_joueur()) {
            if (!p.isIs_dame()) {
                img = new Image("/images/black.png");
            } else {
                img = new Image("/images/macron.jpg");
            }

            map_rectangle.get(Integer.toString(p.getCurrent_case().getPositionI()) + "-" + Integer.toString(p.getCurrent_case().getPositionJ())).setFill(new ImagePattern(img));
            //damier.getChildren().add(map_rectangle.get(Integer.toString(p.getCurrent_case().getPositionI())+"-"+Integer.toString(p.getCurrent_case().getPositionJ())));
        }

        for (Pion p : j1.getPions_joueur()) {
            if (!p.isIs_dame()) {
                img = new Image("/images/white.png");
            } else {
                img = new Image("/images/lepen.jpg");
            }
            map_rectangle.get(Integer.toString(p.getCurrent_case().getPositionI()) + "-" + Integer.toString(p.getCurrent_case().getPositionJ())).setFill(new ImagePattern(img));
            //damier.getChildren().add(map_rectangle.get(Integer.toString(p.getCurrent_case().getPositionI())+"-"+Integer.toString(p.getCurrent_case().getPositionJ())));
        }



    }

    public void creerPartie() throws Exception {
        String[] params = new String[10];

        params[0]=this.portServeur.getText();
        params[1]="127.0.0.1";
        params[2]="PartieDame";

        Serveur.main(params);

        connexionServeur();
    }

    public void connexionServeur() throws RemoteException {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Attention");
        alert.setHeaderText("Remarque");



        String clientPseudo = pseudo.getText();
        //on connecte le client au serveur
        c.connexionServeur(this.ipServeur.getText(), this.portServeur.getText());
        //c.getObjet();
        if(clientPseudo.length() == 0){
            alert.setContentText("Veuillez saisir un pseudo");
            alert.showAndWait();
        }
        else {
            try {
                if (((InterfaceJeuDeDame) c.partieDame).rejoindrePartie(clientPseudo, c)) {
                    this.genererDamier();
                } else {
                    ((InterfaceJeuDeDame) c.partieDame).continuerPartie(clientPseudo, c);
                }
                ((InterfaceJeuDeDame) c.partieDame).actualiserAffichageClient();
                this.j1.setText(((InterfaceJeuDeDame) c.partieDame).getJoueur_1().getNom_joueur());
                this.j2.setText("Attente d'un joueur ");
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    public void genererDamier() throws RemoteException {
        //Si la partie est lancée on initialise le Damier

        int x = 0;
        int y = 9;
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                final Case case_courante = ((InterfaceJeuDeDame) c.partieDame).getCase_x_y(x, y);
                final Rectangle r = new Rectangle(mesure_case, mesure_case);
                r.setX(mesure_case * j);
                r.setY(mesure_case + (mesure_case * i));

                if ((j % 2 == 0 && i % 2 == 0) || (j % 2 != 0 && i % 2 != 0)) {
                    r.setFill(Color.web("#bc9359"));
                } else {
                    r.setFill(Color.web("#886c3b"));
                    if (y >= 6) {
                        Image img = new Image("/images/black.png");
                        r.setFill(new ImagePattern(img));
                    } else if (y <= 3) {
                        Image img = new Image("/images/white.png");
                        r.setFill(new ImagePattern(img));
                    }
                }
                damier.getChildren().add(r);

                genererEvent(case_courante, r);


                if (x != 9) {
                    x++;
                } else {
                    x = 0;
                    y--;
                }
                // On ajoute le Rectangle à l'arraylist
                array_rectangle.add(r);
            }
            Joueur monJoueur = ((InterfaceJeuDeDame) c.partieDame).getJoueur(c);
            if(monJoueur.getNumero_joueur() == 1){
                damier.rotateProperty().set(0);
            } else {

                damier.rotateProperty().set(180);
            }
        }
    }

    public void genererEvent(final Case case_courante, final Rectangle r) {
        // On génère les déclancheurs attachés à ce Rectangle
        r.setOnMouseClicked(new EventHandler<MouseEvent>() { // clic sur la case
            public void handle(MouseEvent event) {

                try {

                    if (((InterfaceJeuDeDame) c.partieDame).selectionnerPion(case_courante.getPositionI(), case_courante.getPositionJ(), c)) {
                        System.out.println(case_courante.getPositionI() + " ; " + case_courante.getPositionJ());
                        selectCase(r);
                    }

                    if (((InterfaceJeuDeDame) c.partieDame).selectionnerCase(case_courante, c)) {
                        selectCase(r);
                    }


                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void selectCase(Rectangle r) {
        for (Rectangle rlist : array_rectangle) { // On enlève le style de tous les rectangles
            rlist.setStyle("-fx-stroke-width: 0;-fx-stroke: transparent;");
        }
        r.setStyle("-fx-stroke-width: 5;-fx-stroke: green;");
    }

    public String getJ1() {
        return this.j1.getText();
    }

    public void savePartie() throws IOException {
        try {

            ((InterfaceJeuDeDame) c.partieDame).savePartie();
            System.out.println("Partie enregitrée");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void loadPartie() throws IOException {
        try {
            String clientPseudo = pseudo.getText();
            creerPartie();

            ((InterfaceJeuDeDame) c.partieDame).loadPartie();
            ((InterfaceJeuDeDame) c.partieDame).continuerPartie(clientPseudo, c);
            System.out.println("Partie chargée");
            refreshDamier(); // TODO : voir comment regénérer le damier après chargement d'une partie
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
