package com.jeux_dames.Tests;

import com.jeux_dames.Classes.Case;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Anthony on 22/03/2017.
 */
class CaseTest {
    @BeforeEach
    void setUp() {

    }

    @AfterEach
    void tearDown() {

    }

    @Test
    void equals() {
        Case a = new Case(0,0,true);
        Case b = new Case(0,0,true);
        Case c = new Case(10,0,true);
        Case d = new Case(0,5,true);

        assertTrue(a.equals(b));
        assertFalse(a.equals(d));
        assertFalse(a.equals(c));
        assertFalse(c.equals(d));
    }

}