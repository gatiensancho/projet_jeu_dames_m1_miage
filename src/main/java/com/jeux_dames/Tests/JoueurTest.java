package com.jeux_dames.Tests;

import com.jeux_dames.Classes.Case;
import com.jeux_dames.Classes.Joueur;
import com.jeux_dames.Classes.Pion;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Anthony on 22/03/2017.
 */
class JoueurTest {

    private Joueur j;
    @BeforeEach
    void setUp() {

        this.j = new Joueur("jean",1, null);
        //On ajoute des pions au joueur qui ont pour case de position 0,0 ; 1,1 ; 2,2 ...
        for (int i=0; i<20; i++){
            j.getPions_joueur().add(new Pion(new Case(i,i,true)));
        }
    }

    @AfterEach
    void tearDown() {

    }

    @Test
    void hasPion() {
        //Ce pion est possèdé par le joueur
        Pion p = new Pion(new Case(0,0,true));
        //ce pion ne l'est pas
        Pion p2 = new Pion(new Case(10,0,true));

        assertTrue(j.hasPion(p.getCurrent_case()));
        assertFalse(j.hasPion(p2.getCurrent_case()));
    }

    @Test
    void debutTour() {

    }

    @Test
    void finTour() {

    }

    @Test
    void isSon_tour() {

    }

}