package com.jeux_dames.Tests;

import com.jeux_dames.Classes.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.ref.SoftReference;
import java.rmi.RemoteException;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Gatien on 17/03/2017.
 */
class PartieTest {

    private Partie partie;
    private ClientDistant clientDistant;
    private ClientDistant clientDistant2;
    private ClientDistant clientDistant3;

    @BeforeEach
    void setUp() {
        try {
            partie = new Partie();
            clientDistant = new ClientPartie();
            clientDistant2 = new ClientPartie();
            clientDistant3 = new ClientPartie();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    @AfterEach
    void tearDown() {

    }

    @Test
    void creerPion() throws RemoteException {
        ArrayList<Pion> liste_pion = partie.creerPion(1);
        assertEquals(20, liste_pion.size(), "Le nombre de pions créé doit être 20");
    }

    @Test
    void avancerPion() {

    }

    @Test
    void rejoindrePartie() throws RemoteException {
        String pseudo = "testPseudo";
        assertTrue(partie.rejoindrePartie(pseudo, clientDistant), "Il faut pouvoir ajouter un client");
        assertTrue(partie.rejoindrePartie(pseudo, clientDistant), "Il faut pouvoir ajouter un deuxième client");
        assertFalse(partie.rejoindrePartie(pseudo, clientDistant), "Il ne faut pas pouvoir ajouter un troisième client");
    }

    @Test
    void getJoueurCase() {

    }

    @Test
    void getTourJoueur() {

    }

    @Test
    void registerClient() {

    }

    @Test
    void mettreAjourClient() {

    }

    @Test
    void mettreAjourDamierClient() {

    }

    @Test
    void selectionnerPion() throws RemoteException {
        String pseudo = "testPseudo";

        //on fait rejoindre deux clients
        partie.rejoindrePartie(pseudo, clientDistant);
        partie.rejoindrePartie(pseudo, clientDistant2);

        //On test si le joueur 1 à bien la main sur la partie
        assertEquals(clientDistant,partie.getTourJoueur().getClient());
        //Test de selection d'une case ne contenant pas de pion
        assertFalse(this.partie.selectionnerPion(0,1,clientDistant));
        //Test de la selection d'une case ou il y a un pion du joueur qui a la main sur le damier
        assertTrue(this.partie.selectionnerPion(0,0,clientDistant));
        //Test de la selection d'une case ou il y a un pion du joueur qui n'a pas la main sur le damier
        assertFalse(this.partie.selectionnerPion(0,8,clientDistant));

        //Test de la selection d'une case par un joueur qui n'a pas la main sur le damier
        assertFalse(this.partie.selectionnerPion(0,8,clientDistant2));
    }

    @Test
    void getActionPossiblePion() throws RemoteException {
        //Il reste les tests de prise de pion a faire
        //A voir avec la fonction prise de pion
        String pseudo = "testPseudo";

        //on fait rejoindre deux clients
        partie.rejoindrePartie(pseudo, clientDistant);
        partie.rejoindrePartie(pseudo, clientDistant2);
        //On selectionne un pion selectionnable
        partie.selectionnerPion(0,0,clientDistant);

        assertEquals(partie.getActionPossible(new Case(1,3,true)),-1);

        //On selectionne un pion selectionnable
        partie.selectionnerPion(1,3,clientDistant);

        //avancée simple
        assertEquals(partie.getActionPossible(new Case(2,4,true)),1);

        //test de prise sans pion
        assertEquals(partie.getActionPossible(new Case(4,6,true)),-1);


        //On ajoute un pion adverse entre
        partie.getJoueur_2().getPions_joueur().add(new Pion(new Case(2,4,true)));

        //test de prise avec pion adverse
        assertEquals(partie.getActionPossible(new Case(3,5,true)),2);

        //On selectionne le pion précédent
        partie.selectionnerPion(3,5,clientDistant);

        //test retour arrière
        assertEquals(partie.getActionPossible(new Case(2,4,true)),-1);

        //test sur un pion adverse
        assertEquals(partie.getActionPossible(new Case(4,6,true)),-1);
    }

    @Test
    void deplacerPion() throws RemoteException { // opitimisation (possible ?) réutilisation de code du test de la méthode selectionnerPion()
        String pseudo = "testPseudo";

        //on fait rejoindre deux clients
        partie.rejoindrePartie(pseudo, clientDistant);
        partie.rejoindrePartie(pseudo, clientDistant2);

        //On test si le joueur 1 à bien la main sur la partie
        assertEquals(clientDistant,partie.getTourJoueur().getClient());

        //Test de la selection d'une case ou il y a un pion du joueur qui a la main sur le damier
        assertTrue(this.partie.selectionnerPion(0,2,clientDistant));

        /// TODO Gatien 29/04/2017 : trouver un moyen de terminer le test : simuler la selection d'une Case et non d'une coordonnées pour executer la fonction deplacerPion(Case c)
    }

    @Test
    void getCase_x_y() {

    }

    @Test
    void getCase_rectangle() {

    }



}